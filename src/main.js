const express = require('express')
const Prometheus = require('prom-client');

const app = express()
const port = 3000

const counter = new Prometheus.Counter({
  name: 'open_count',
  help: 'home_page_open_count',
});

const gauge = new Prometheus.Gauge({
  name: 'random',
  help: 'some_random_metrics',
});

const gaugeUpdate = () => {
  const rnd = Math.floor(Math.random() * 100);
  gauge.set(rnd);

  setTimeout(() => {
    gaugeUpdate();
  }, 3000);
};
gaugeUpdate();

app.get('/', (req, res) => {
  counter.inc();

  res.send('Node metrics test application')
})

app.get('/check', (req, res) => {
  res.send('OK')
})

app.get('/metrics', async(req, res) => {
  res.set('Content-Type', Prometheus.register.contentType)
  res.end(await Prometheus.register.metrics())
});

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})

