# README #

Test nodejs metrics project

### Run ###

`docker-compose up -d`

### Routes ###

http://127.0.0.1:3000 - home page
http://127.0.0.1:3000/check - live check
http://127.0.0.1:3000/metrics - metrics export page
http://127.0.0.1:3100 - grafana
http://127.0.0.1:9090 - prometheus


### Grafana access ###

* Use `admin` user name and empty password for first access. Then reopen page.
* Demo dashboard can be found in the menu `Dashboards` -> `Manage` 
